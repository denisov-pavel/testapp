import {
  FETCH_IMAGES_START,
  FETCH_IMAGES_SUCCES,
  FETCH_IMAGES_ERROR
} from './actionsType';

export function fetchImages() {
  return async (dispatch,getState) => {
    dispatch(fetchImagesStart())
    const prev_images = getState().image.images
    const page = getState().image.page
    const images = [...prev_images]
    try {
      const response = await fetch(`https://api.unsplash.com/photos/?client_id=cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0&page=${page}&per_page=20`);
      const json = await response.json();
      for (const value of json) {
        images.push({
          imageSmall: value.urls.small,
          imageFull: value.urls.full,
          id: value.id,
          author: value.user.name
        })
      }
      dispatch(fetchImagesSucces(images,page))
    }
    catch(e) {
      dispatch(fetchImagesError())
    }
  }
}

export function fetchImagesError() {
  return {
    type: FETCH_IMAGES_ERROR
  }
}


export function fetchImagesSucces(images,page) {
  return {
    type: FETCH_IMAGES_SUCCES,
    images,
    page
  }
}

export function fetchImagesStart() {
  return {
    type: FETCH_IMAGES_START
  }
}