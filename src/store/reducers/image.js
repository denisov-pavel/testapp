import {
  FETCH_IMAGES_SUCCES, 
  FETCH_IMAGES_START,
  FETCH_IMAGES_ERROR
} from '../actions/actionsType'
const initialState = {
  images: [],
  page: 1,
  loading: false,
  error: false
}

export default function imageReducer(state = initialState,action) {
  switch(action.type) {
    case FETCH_IMAGES_START:
      return {
        ...state,
        loading: true
      }
    case FETCH_IMAGES_SUCCES:
      return {
        ...state,
        images: action.images,
        page: action.page +1,
        loading: false
      }
    case FETCH_IMAGES_ERROR:
      return {
        ...state,
        error: true,
        page: 1,
      }
    default:
      return state
  }
}