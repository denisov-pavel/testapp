import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStackNavigator, createAppContainer } from 'react-navigation';
import Images from './components/Images';
import Image from './components/Image';
import configureStore from './store/store'


export default class App extends Component {
  render() {
    const MainNavigator = createStackNavigator({
      MainScreen: { screen: Images },
      Image: { screen: Image },
    })
    const AppContainer = createAppContainer(MainNavigator);
    return (
      <Provider store={configureStore()}>
        <AppContainer />
      </Provider>
    );
  }
}