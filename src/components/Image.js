import React, { Component } from 'react'
import { View, Image,StyleSheet } from 'react-native';
export default class Images extends Component {
  static navigationOptions = {
    header: null,
    gesturesEnabled: true
  };
  render() {
    const { navigation } = this.props;
    const imageFull = navigation.getParam('imageFull');
    return (
      <View
        style={styles.container}
      >
        <Image
          resizeMode="contain"
          source={{ uri: imageFull }}
          style={styles.image}
        />
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#202124',
  },
  image: {
    width: '100%',
    height: '100%'
  }
})