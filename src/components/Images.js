import React, { Component } from 'react'
import { 
  StyleSheet, 
  View, Image, 
  Text, Dimensions, 
  FlatList, TouchableOpacity, 
  ActivityIndicator, 
  Alert 
} from 'react-native';
import { connect } from "react-redux";
import { fetchImages } from '../store/actions/image'
class Images extends Component {
  static navigationOptions = {
    title: 'Images',
    headerStyle: {
      backgroundColor: '#202124',
      borderBottomColor: 'rgba(0,0,0,.5)',
      borderBottomWidth: 1,
    },
    headerTitleStyle: {
      textAlign: "center",
      flex: 1,
    },
    headerTintColor: '#fafafa',
  };
  constructor(props) {
    super(props);
    this.state = {
      imageWidth: Dimensions.get('window').width / 2,
      imageHeigh: Dimensions.get('window').height / 3,
      numColumns: 2,
    }
  }
  componentDidMount() {
    this.props.fetchImages()
  }
  onLayout() {
    const { width, height } = Dimensions.get('window')
    if (width > height) {
      this.setState({
        numColumns: 4,
        horizontal: true,
        imageWidth: Dimensions.get('window').width / 4,
      })
    } else {
      this.setState({
        numColumns: 2,
        horizontal: false,
        imageWidth: Dimensions.get('window').width / 2,
      })
    }
  }
  render() {
    const { navigate } = this.props.navigation;
    if (this.props.error) {
      Alert.alert(
        'Error',
        'Something went wrong try restarting app?',
        [
          { text: 'No', onPress: () => false, style: 'cancel' },
          { text: 'Yes', onPress: () => this.forceUpdate() },
        ],
        { cancelable: false }
      )
    }
    return (
      <View
        style={styles.container}
        onLayout={() => this.onLayout()}
      >
        {
          this.props.loading && this.props.images.length === 0 ?
            <ActivityIndicator
              size={200}
              color="#0000ff"
            />
            : <FlatList
                key={(this.state.horizontal ? 'h' : 'v')}
                numColumns={this.state.numColumns}
                data={this.props.images}
                renderItem={({ item, index }) => {
                  return (
                    <View
                      style={styles.imageContainer}>
                      <TouchableOpacity onPress={() => navigate('Image', { imageFull: item.imageFull })}>
                        <Image
                          key={index}
                          source={{ uri: item.imageSmall }}
                          style={{ width: this.state.imageWidth, height: this.state.imageHeigh }}
                          resizeMode="cover"
                        />
                      </TouchableOpacity>
                      <Text style={styles.authorText}>{item.author}</Text>
                    </View>
                  )
                }}
                keyExtractor={(item,index) => item.id + index}
                onEndReached={() => this.props.fetchImages()}
                onEndReachedThreshold={2}
              />
        }
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    images: state.image.images,
    loading: state.image.loading,
    page: state.image.page,
    error: state.image.error
  }
}

function mapDispatchToProps(dispatch) {
  return {
    fetchImages: page => dispatch(fetchImages(page))
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#202124',
    justifyContent: 'center'
  },
  imageContainer: {
    borderRightWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#fff'
  },
  authorText: {
    paddingTop: 10,
    paddingBottom: 10,
    textAlign: 'center',
    color: '#fafafa',
    backgroundColor: '#202124'
  }
})

export default connect(mapStateToProps, mapDispatchToProps)(Images)